'use strict';

const fs = require('fs');
const colors = require('colors');

const Game = require('./src/game');

let fieldData = process.argv[2],
    scriptData = process.argv[3];

if (fieldData === undefined || scriptData === undefined) {
    console.log('ERROR: Need to provide path to field and script files.'.red);
    process.exit();
}

let game = new Game();

loadFieldAndScriptFiles(process.argv[2], process.argv[3]);

/***
 *
 Load the passed in field and script paths.
 */
function loadFieldAndScriptFiles(fieldFile, scriptFile) {
    console.log('Loading field file:'.yellow, fieldFile);
    let fieldDataPromise = new Promise((resolve, reject) => {
        fs.readFile(fieldFile, 'utf8', (err, data) => {
            if (data) {
                fieldData = data;
                resolve(data);
            } else { reject('Could not load specified field file.'); }
        });
    });

    console.log('Loading script file:'.yellow, scriptFile);
    let scriptDataPromise = new Promise((resolve, reject) => {
        fs.readFile(scriptFile, 'utf8', (err, data) => {
            if (data) {
                scriptData = data;
                resolve(data);
            } else { reject('Could not load specified script file.'); }
        });
    });

    Promise.all([
        fieldDataPromise,
        scriptDataPromise
    ]).then(() => {
        setupGame();
    }).catch((err) => {
        console.log('ERROR:'.red, err.red);
        process.exit();
    });
}

/***
 *
 Set up and initialize the game's data.
 */
function setupGame() {
    console.log('\nDone loading files...'.yellow);
    game.initializeFieldAndScriptData(fieldData, scriptData);
    game.setMinePositionsAndCurrentScore();
    game.runScriptSteps();
}
