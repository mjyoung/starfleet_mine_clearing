## Starfleet Mine Clearing Exercise Evaluator

### How to run

Using a Node version compatible with ES2015 (I'm on Node v5.8.0):

1. Clone down and cd into directory
2. npm install (or [yarn install](https://yarnpkg.com/blog/) if you have it)
3. `node app.js <field filepath> <script filepath>`
    * ex: `node app.js scripts/example1.field scripts/example1.script`
    * Or, to run them all, `npm run test`
    * Or, to run them individually, `npm run test:1`, `npm run test:2`, etc.

### Directory structure

```
app.js (entry point)
src/ (logic related to running the simulation)
    game.js (Game class for maintaining game state and methods for running the simulation)
    ship.js (Ship class for methods related to moving and firing the ship)
    mine_dictionary.js (code to generate a hash table of letters with their corresponding z-depths)
scripts/ (contains the example scripts files to test the simulator)
    example1.field
    example1.script
    ...
    example5.field
    example5.script
coding-exercise-swen_(2).pdf (the prompt that the code is based on)
```

### Third party libraries used

* [colors.js](https://github.com/marak/colors.js/) - Provides color to the console output (oooh ahhh pretty).
