'use strict';

const Ship = require('./ship');
const ship = new Ship();
const mineDictionary = require('./mine_dictionary');

class Game {
    constructor() {
        this.currentField = null;
        this.scriptData = null;
        this.totalSteps = null;
        this.minePositions = [];
        this.currentScore = null;
        this.fireCount = 0;
        this.moveCount = 0;
    }

    /***
     *
     Convert the provided utf8 fieldData and scriptData into arrays.
     */
    initializeFieldAndScriptData(fieldData, scriptData) {
        console.log('\n=== FIELD DATA ===');
        fieldData = fieldData.split('\n').map(data => {
            return data.split('');
        });
        this.currentField = fieldData;
        console.log(fieldData);

        console.log('\n=== SCRIPT DATA ===');
        scriptData = scriptData.split('\n').map(data => {
            return data.split(' ');
        });
        this.scriptData = scriptData;
        this.totalSteps = scriptData.length;
        console.log(scriptData);
    }

    /***
     *
     Set mine positions based on fieldData.
     */
    setMinePositionsAndCurrentScore() {
        const shipPositionY = (this.currentField.length - 1) / 2,
              shipPositionX = (this.currentField[0].length - 1) / 2;
        for(let rowIndex = 0; rowIndex < this.currentField.length; rowIndex++) {
            for(let colIndex = 0; colIndex < this.currentField[rowIndex].length; colIndex++) {
                const regExp = /[A-z]/g;
                const char = this.currentField[rowIndex][colIndex];
                if (char.match(regExp)) {
                    this.minePositions.push({ xFromShip: colIndex - shipPositionX, yFromShip: shipPositionY - rowIndex, z: mineDictionary[char] });
                }
            }
        }
        this.currentScore = this.minePositions.length * 10;
        console.log('\n=== MINE POSITIONS ===');
        console.log(this.minePositions);
    }

    /***
     *
     Iterate through script steps.
     */
    runScriptSteps() {
        console.log('\nExecuting steps...\n'.yellow);

        for (let currentStep = 1; currentStep <= this.totalSteps; currentStep++) {

            // Output current step number
            console.log((`\nStep ${currentStep} \n`.blue));

            // Output current field
            for (let rowData of this.currentField) {
                console.log(rowData.join(''));
            }

            // Output step instructions
            console.log('\n' + this.scriptData[currentStep - 1].join(' ').yellow + '\n');

            // Run action and update field
            this.runAction(currentStep);
            this.updateField(currentStep);

            // Output resultant field
            for (let rowData of this.currentField) {
                console.log(rowData.join(''));
            }

            // Check if game over
            let gameOverText = this.calculateGameOver(currentStep);
            if (gameOverText) {
                console.log('\n' + gameOverText + '\n');
                process.exit();
            }
        }
    }

    /***
     *
     Run a single step / action.
     */
    runAction(currentStep) {
        for (let instruction of this.scriptData[currentStep - 1]) {
            switch (instruction) {
                case 'north':
                    this.minePositions = ship.moveNorth(this.minePositions);
                    this.moveCount += 1;
                    break;
                case 'south':
                    this.minePositions = ship.moveSouth(this.minePositions);
                    this.moveCount += 1;
                    break;
                case 'east':
                    this.minePositions = ship.moveEast(this.minePositions);
                    this.moveCount += 1;
                    break;
                case 'west':
                    this.minePositions = ship.moveWest(this.minePositions);
                    this.moveCount += 1;
                    break;
                case 'alpha':
                    this.minePositions = ship.fireAlpha(this.minePositions);
                    this.fireCount += 1;
                    break;
                case 'beta':
                    this.minePositions = ship.fireBeta(this.minePositions);
                    this.fireCount += 1;
                    break;
                case 'gamma':
                    this.minePositions = ship.fireGamma(this.minePositions);
                    this.fireCount += 1;
                    break;
                case 'delta':
                    this.minePositions = ship.fireDelta(this.minePositions);
                    this.fireCount += 1;
                    break;
            }
        }
    }

    /***
     *
     Update the currentField.
     */
    updateField(currentStep) {
        let maxX = 0, maxY = 0;
        for (let mine of this.minePositions) {
            if (Math.abs(mine.xFromShip) > maxX) {
                maxX = Math.abs(mine.xFromShip);
            }
            if (Math.abs(mine.yFromShip) > maxY) {
                maxY = Math.abs(mine.yFromShip);
            }
        }
        const colSize = maxX * 2 + 1,
              rowSize = maxY * 2 + 1;

        this.currentField = [];
        for (let i = 0; i < rowSize; i++) {
            let row = [];
            for (let j = 0; j < colSize; j++) {
                row.push('.');
            }
            this.currentField.push(row);
        }

        const shipPositionY = (this.currentField.length - 1) / 2,
              shipPositionX = (this.currentField[0].length - 1) / 2;

        for (let mine of this.minePositions) {
            this.currentField[shipPositionY - mine.yFromShip][shipPositionX - mine.xFromShip] = mineDictionary[mine.z + currentStep];
            if (mine.z + currentStep >= 0) {
                this.currentField[shipPositionY - mine.yFromShip][shipPositionX - mine.xFromShip] = '*';
            }
        }
    }

    /***
     *
     Calculate if game is over and return gameOverText.
     */
    calculateGameOver(currentStep) {
        let gameOverText;

        for (let rowData of this.currentField) {
            if (rowData.join('').match(/[*]/g)) {
                this.currentScore = 0;
                gameOverText = `Fail! (${this.currentScore}) - Passed a mine`.red;
            }
        }

        if (currentStep === this.totalSteps && this.minePositions.length !== 0) {
            this.currentScore = 0;
            gameOverText = `Fail! (${this.currentScore}) - Script completed but mines still remaining`.red;
        }

        if (this.minePositions.length === 0) {
            this.currentScore = this.calculateCurrentScore(currentStep);
            gameOverText = `Pass! (${this.currentScore})`.green;
        }

        return gameOverText;
    }

    /***
     *
     Calculate the current score for the passed game.
     */
    calculateCurrentScore(currentStep) {
        if (currentStep < this.totalSteps) {
            return 1;
        }

        const initialMineCount = this.currentScore / 10;
        const maxPointsReducedFromFiring = initialMineCount * 5;
        const maxPointsReducedFromMoving = initialMineCount * 3;

        let pointsReducedFromFiring = this.fireCount * 5 > maxPointsReducedFromFiring ? maxPointsReducedFromFiring : this.fireCount * 5,
            pointsReducedFromMoving = this.moveCount * 2 > maxPointsReducedFromMoving ? maxPointsReducedFromMoving : this.moveCount * 2;

        return (this.currentScore - pointsReducedFromFiring - pointsReducedFromMoving);
    }

}

module.exports = Game;
