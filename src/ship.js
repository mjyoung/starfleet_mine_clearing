'use strict';

class Ship {
    constructor() {
    }

    moveNorth(minePositions) {
        for (let mine of minePositions) {
            mine.yFromShip -= 1;
        }
        return minePositions;
    }

    moveSouth(minePositions) {
        for (let mine of minePositions) {
            mine.yFromShip += 1;
        }
        return minePositions;
    }

    moveWest(minePositions) {
        for (let mine of minePositions) {
            mine.xFromShip -= 1;
        }
        return minePositions;
    }

    moveEast(minePositions) {
        for (let mine of minePositions) {
            mine.xFromShip += 1;
        }
        return minePositions;
    }

    fireAlpha(minePositions) {
        const fireLocations = [
            { x: -1, y: -1 },
            { x: -1, y: 1 },
            { x: 1, y: -1 },
            { x: 1, y: 1 }
        ];
        minePositions = this.fireWeapon(fireLocations, minePositions);
        return minePositions;
    }

    fireBeta(minePositions) {
        const fireLocations = [
            { x: -1, y: 0 },
            { x: 0, y: -1 },
            { x: 0, y: 1 },
            { x: 1, y: 0 }
        ];
        minePositions = this.fireWeapon(fireLocations, minePositions);
        return minePositions;
    }

    fireGamma(minePositions) {
        const fireLocations = [
            { x: -1, y: 0 },
            { x: 0, y: 0 },
            { x: 1, y: 0 }
        ];
        minePositions = this.fireWeapon(fireLocations, minePositions);
        return minePositions;
    }

    fireDelta(minePositions) {
        const fireLocations = [
            { x: 0, y: -1 },
            { x: 0, y: 0 },
            { x: 0, y: 1 }
        ];
        minePositions = this.fireWeapon(fireLocations, minePositions);
        return minePositions;
    }

    fireWeapon(fireLocations, minePositions) {
        for (let i = 0; i < minePositions.length; i++) {
            for (let fireLocation of fireLocations) {
                if (minePositions[i].xFromShip === fireLocation.x &&
                    minePositions[i].yFromShip === fireLocation.y) {
                    minePositions.splice(i, 1);
                    break;
                }
            }
        }
        return minePositions;
    }
}

module.exports = Ship;
