'use strict';

function generateCharacterDictionary(char1, char2) {
    let hash = {};
    for (let i = char1.charCodeAt(0); i <= char2.charCodeAt(0); i++) {
        let char = {};
        let diff = i * (-1) + 38;
        if (char1.charCodeAt(0) > 90) { // A-Z is 65-90. a-z is 97 to 122.
            diff = i * (-1) + 96;
        }
        hash[String.fromCharCode(i)] = diff;
        hash[diff] = String.fromCharCode(i);
    }
    return hash;
}

const dictionary = Object.assign(generateCharacterDictionary('a', 'z'), generateCharacterDictionary('A', 'Z'));

module.exports = dictionary;
